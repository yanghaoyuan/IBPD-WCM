package com.ibpd.shopping.service.ordertmp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.OrderTmpEntity;
@Service("orderTmpService")
public class OrderTmpServiceImpl extends BaseServiceImpl<OrderTmpEntity> implements IOrderTmpService {
	public OrderTmpServiceImpl(){
		super();
		this.tableName="OrderTmpEntity";
		this.currentClass=OrderTmpEntity.class;
		this.initOK();
	}

	public List<OrderTmpEntity> getOrderTmpList(String mOrderId) {
		return getList("from "+getTableName()+" where tmpOrderNumber like '%"+mOrderId+"%'",null);
	}
}
