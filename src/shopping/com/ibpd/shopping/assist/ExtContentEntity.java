package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.henuocms.entity.ContentEntity;

public class ExtContentEntity extends ContentEntity {

	private String  nodeName="";
	private Long nodeId=-1L;
	private String nodeBackgroundImage="";
	private String nodeLogo="";
	private String nodeDescription="";
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public Long getNodeId() {
		return nodeId;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeBackgroundImage() {
		return nodeBackgroundImage;
	}
	public void setNodeBackgroundImage(String nodeBackgroundImage) {
		this.nodeBackgroundImage = nodeBackgroundImage;
	}
	public String getNodeLogo() {
		return nodeLogo;
	}
	public void setNodeLogo(String nodeLogo) {
		this.nodeLogo = nodeLogo;
	}
	public ExtContentEntity(ContentEntity c){
		try {
			InterfaceUtil.swap(c, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setNodeDescription(String nodeDescription) {
		this.nodeDescription = nodeDescription;
	}
	public String getNodeDescription() {
		return nodeDescription;
	}
}
