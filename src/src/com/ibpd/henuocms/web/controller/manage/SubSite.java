package com.ibpd.henuocms.web.controller.manage;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.Pinyin4jUtil;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.PageTemplateExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;

@Controller
public class SubSite extends BaseController {
	@RequestMapping("Manage/SubSite/index.do")
	public String index(org.springframework.ui.Model model,HttpServletRequest req) throws IOException{
		model.addAttribute(PAGE_TITLE,"站点管理");
		return "manage/subSite/index";
	}
	@RequestMapping("Manage/SubSite/toAdd.do") 
	public String toAdd(Model model,HttpServletRequest req,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		model.addAttribute(PAGE_TITLE,"添加站点");
		List<PageTemplateExtEntity> sitePageTemplateList=null;
		List<StyleTemplateEntity> siteStyleTemplateList=null;
		List<PageTemplateExtEntity> nodePageTemplateList=null;
		List<StyleTemplateEntity> nodeStyleTemplateList=null;
		List<PageTemplateExtEntity> contentpageTemplateList=null;
		List<StyleTemplateEntity> contentStyleTemplateList=null;
		List<PageTemplateExtEntity> commentpageTemplateList=null;
		List<StyleTemplateEntity> commentStyleTemplateList=null;
		IPageTemplateService pageTempServ=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		sitePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_SITE);
		nodePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_NODE);
		contentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_CONTENT);
		commentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_COMMENT);
		
		IStyleTemplateService styleTempServ=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		siteStyleTemplateList=styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		nodeStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		contentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		commentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		model.addAttribute("sitePageTemplateList",sitePageTemplateList);
		model.addAttribute("siteStyleTemplateList",siteStyleTemplateList);
		model.addAttribute("nodePageTemplateList",nodePageTemplateList);
		model.addAttribute("nodeStyleTemplateList",nodeStyleTemplateList);
		model.addAttribute("contentpageTemplateList",contentpageTemplateList);
		model.addAttribute("contentStyleTemplateList",contentStyleTemplateList);
		model.addAttribute("commentpageTemplateList",commentpageTemplateList);
		model.addAttribute("commentStyleTemplateList",commentStyleTemplateList);

		return "manage/subSite/add";
	}
	@RequestMapping("Manage/SubSite/toFtpConfig.do") 
	public String toFtpConfig(Long siteId,Model model,HttpServletRequest req,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		model.addAttribute(PAGE_TITLE,"站点FTP参数设置");
		if(siteId==null || siteId<0){
			model.addAttribute(ERROR_MSG,MSG_PARAMERROR_MSG);
			return ERROR_PAGE;
		}
		ISubSiteService siteServ=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity site=siteServ.getEntityById(siteId);
		if(site==null){
			model.addAttribute(ERROR_MSG,"没有该站点");
			return ERROR_PAGE;
		}
		model.addAttribute("site",site);
		return "manage/subSite/ftpConfig";
	}
	@RequestMapping("Manage/SubSite/doFtpConfig.do") 
	public void doFtpConfig(Long siteId,String ftpAddress,String ftpPort,String ftpUserName,String ftpPassword,HttpServletRequest req,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(siteId==null){
			printParamErrorMsg(resp);
			return;
		}
		if(StringUtils.isBlank(ftpAddress) || StringUtils.isBlank(ftpPort) || StringUtils.isBlank(ftpUserName) || StringUtils.isBlank(ftpPassword)){
			super.printMsg(resp, "-2", "-1", "所有参数都不能为空");
			return;
		}
		ISubSiteService siteServ=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity site=siteServ.getEntityById(siteId);
		if(site==null){
			super.printMsg(resp, "-2", "-1", "没有该站点");
			return;
		}
		site.setFtpAddress(ftpAddress);
		site.setFtpPort(ftpPort);
		site.setFtpUserName(ftpUserName);
		site.setFtpPassword(ftpPassword);
		siteServ.saveEntity(site);
		super.printMsg(resp, "99", "99", "保存成功");
	}
	@RequestMapping("Manage/SubSite/toEdit.do")
	public String toEdit(Model model,Long id,HttpServletRequest req,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null){
			model.addAttribute(ERROR_MSG,MSG_PARAMERROR_MSG);
			return this.ERROR_PAGE;
		}
		List<PageTemplateExtEntity> sitePageTemplateList=null;
		List<StyleTemplateEntity> siteStyleTemplateList=null;
		List<PageTemplateExtEntity> nodePageTemplateList=null;
		List<StyleTemplateEntity> nodeStyleTemplateList=null;
		List<PageTemplateExtEntity> contentpageTemplateList=null;
		List<StyleTemplateEntity> contentStyleTemplateList=null;
		List<PageTemplateExtEntity> commentpageTemplateList=null;
		List<StyleTemplateEntity> commentStyleTemplateList=null;
		IPageTemplateService pageTempServ=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		sitePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_SITE);
		nodePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_NODE);
		contentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_CONTENT);
		commentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_COMMENT);
		
		IStyleTemplateService styleTempServ=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		siteStyleTemplateList=styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		nodeStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		contentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		commentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		model.addAttribute("sitePageTemplateList",sitePageTemplateList);
		model.addAttribute("siteStyleTemplateList",siteStyleTemplateList);
		model.addAttribute("nodePageTemplateList",nodePageTemplateList);
		model.addAttribute("nodeStyleTemplateList",nodeStyleTemplateList);
		model.addAttribute("contentpageTemplateList",contentpageTemplateList);
		model.addAttribute("contentStyleTemplateList",contentStyleTemplateList);
		model.addAttribute("commentpageTemplateList",commentpageTemplateList);
		model.addAttribute("commentStyleTemplateList",commentStyleTemplateList);
		model.addAttribute(PAGE_TITLE,"编辑站点");
		ISubSiteService rs=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity ce=rs.getEntityById(id);
		model.addAttribute("site",ce); 
		return "manage/subSite/edit";
	}
	@RequestMapping("Manage/SubSite/doAdd.do")
	public void doAdd(SubSiteEntity site,HttpServletResponse resp,HttpServletRequest req) throws IOException{
		if(site==null){
			printParamErrorMsg(resp);
			return;
		}
		if(StringUtils.isBlank(site.getSiteName())){
			super.printMsg(resp, "-2", "-1", "名称不能为空");
			return;
		}
		site.setOrder(site.getOrder()==null?0:site.getOrder());
		site.setStartDate(site.getStartDate()==null?new Date():site.getStartDate());
		site.setEndDate(site.getEndDate()==null?new Date():site.getEndDate());
		site.setEndDate(org.apache.commons.lang.time.DateUtils.addYears(site.getEndDate(), 1));
		site.setCreateUser(SysUtil.getCurrentLoginedUserName(req.getSession()));
		site.setCreateDate(new Date());
		site.setDirectory(Pinyin4jUtil.getPinYin(site.getSiteName()));
		ISubSiteService rs=(ISubSiteService) getService(SubSiteServiceImpl.class);
		rs.saveEntity(site);
		String msg="";
		String realDir=IbpdCommon.getInterface().getSubSiteRealDir(site.getId());
		File f=new File(realDir);
		if(f.exists()){
			msg="保存成功,但站点目录已经存在";
		}else{
			if(!f.mkdir()){
				msg="保存成功,但站点目录未创建成功";
			}else{
				msg="保存成功";
			}
		}
		super.printMsg(resp, "1", "1", msg);		
	}
	@RequestMapping("Manage/SubSite/doEdit.do")
	public void doEdit(SubSiteEntity site,HttpServletResponse resp) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(site==null){
			printParamErrorMsg(resp);
			return;
		}
		if(site.getId()==null || site.getId()<=0){
			super.printMsg(resp, "-2", "-1", MSG_PARAMERROR_MSG);
			return;
		}
		if(StringUtils.isBlank(site.getSiteName())){
			super.printMsg(resp, "-3", "-1", "名称不能为空");
			return;
		}
		site.setOrder(site.getOrder()==null?0:site.getOrder());
		
		ISubSiteService rs=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity dbRole=rs.getEntityById(site.getId());
		super.swap(site, dbRole);
		rs.saveEntity(dbRole);
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/SubSite/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		ISubSiteService rs=(ISubSiteService) getService(SubSiteServiceImpl.class);
		rs.batchDel(ids);
		RecycleNode rn=new RecycleNode();
		String[] idss=ids.split(",");
		String nodeIds="";
		for(String id:idss){
			id=id.trim();
			if(StringUtils.isNumeric(id)){
				INodeService nodeServ=(INodeService) getService(NodeServiceImpl.class);
				List<NodeEntity> nodeList=nodeServ.getList("from "+nodeServ.getTableName()+" where subSiteId="+id,null);
				if(nodeList!=null){
					for(NodeEntity n:nodeList){
						nodeIds=nodeIds+","+n.getId();
					}
				}
			}
		}
		rn.doDel(nodeIds, resp);//执行删除站点下的栏目等信息
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/SubSite/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryData){
		ISubSiteService roleService=(ISubSiteService)getService(SubSiteServiceImpl.class);
		super.getList(req, roleService, resp, order, page, rows, sort, queryData);
	}
	@RequestMapping("Manage/SubSite/props.do")
	public String props(String id,Model model){
		if(id==null || id.trim().length()==0){
			model.addAttribute(ERROR_MSG,MSG_PARAMERROR_MSG);
			return this.ERROR_PAGE;
		}
		List<PageTemplateExtEntity> sitePageTemplateList=null;
		List<StyleTemplateEntity> siteStyleTemplateList=null;
		List<PageTemplateExtEntity> nodePageTemplateList=null;
		List<StyleTemplateEntity> nodeStyleTemplateList=null;
		List<PageTemplateExtEntity> contentpageTemplateList=null;
		List<StyleTemplateEntity> contentStyleTemplateList=null;
		List<PageTemplateExtEntity> commentpageTemplateList=null;
		List<StyleTemplateEntity> commentStyleTemplateList=null;
		IPageTemplateService pageTempServ=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		sitePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_SITE);
		nodePageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_NODE);
		contentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_CONTENT);
		commentpageTemplateList=pageTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", "type="+PageTemplateEntity.TEMPLATE_TYPE_COMMENT);
		
		IStyleTemplateService styleTempServ=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		siteStyleTemplateList=styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		nodeStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		contentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		commentStyleTemplateList=siteStyleTemplateList;//styleTempServ.getList(SysUtil.getManagedSiteIds(), null, 1000, 0, "order asc", null);
		model.addAttribute("sitePageTemplateList",sitePageTemplateList);
		model.addAttribute("siteStyleTemplateList",siteStyleTemplateList);
		model.addAttribute("nodePageTemplateList",nodePageTemplateList);
		model.addAttribute("nodeStyleTemplateList",nodeStyleTemplateList);
		model.addAttribute("contentpageTemplateList",contentpageTemplateList);
		model.addAttribute("contentStyleTemplateList",contentStyleTemplateList);
		model.addAttribute("commentpageTemplateList",commentpageTemplateList);
		model.addAttribute("commentStyleTemplateList",commentStyleTemplateList);
		if(StringUtils.isBlank(id)){
			model.addAttribute(ERROR_MSG,"ID为空");
			return ERROR_PAGE;
		}
		if(!StringUtils.isNumeric(id)){
			model.addAttribute(ERROR_MSG,"ID错误");
			return ERROR_PAGE;
		}
		ISubSiteService siteService = (ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity site=siteService.getEntityById(Long.valueOf(id));
		site.setFtpPassword("********");
		model.addAttribute("site",site);
		return "manage/subSite/props";
	}
	@RequestMapping("Manage/SubSite/saveProp.do")
	public void saveProps(Long id, String field, String value,
			HttpServletResponse resp) throws NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		if (id != null && id != -1) {
			ISubSiteService siteService = (ISubSiteService) getService(SubSiteServiceImpl.class);
			SubSiteEntity node = siteService.getEntityById(id);
			if (node != null) {
				// 先确定参数类型
				Method tmpMethod = node.getClass().getMethod(
						"get" + field.substring(0, 1).toUpperCase()
								+ field.substring(1), new Class[] {});
				Method method = node.getClass().getMethod(
						"set" + field.substring(0, 1).toUpperCase()
								+ field.substring(1),
						new Class[] { tmpMethod.getReturnType() });
				Object val = getValue(value, tmpMethod.getReturnType()
						.getSimpleName());
				method.invoke(node, val);
				siteService.saveEntity(node);
				this.makeStaticPage(MakeType.站点, id, null, null);
			}
		}
	}
}
