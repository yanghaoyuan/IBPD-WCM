package com.ibpd.henuocms.web.controller.manage;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.FileTypeEntity;
import com.ibpd.henuocms.service.fileType.FileTypeServiceImpl;
import com.ibpd.henuocms.service.fileType.IFileTypeService;

@Controller
public class FileType extends BaseController {
	private IFileTypeService fileTypeService=null;
	@RequestMapping("Manage/FileType/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp){
		fileTypeService=(IFileTypeService) ((fileTypeService==null)?ServiceProxyFactory.getServiceProxy(FileTypeServiceImpl.class):fileTypeService);
		super.getList(req, fileTypeService, resp, null,-1, -1, null, null);
	}
	@RequestMapping("Manage/FileType/init.do")
	public void init(HttpServletRequest req,HttpServletResponse resp){
		fileTypeService=(IFileTypeService) ((fileTypeService==null)?ServiceProxyFactory.getServiceProxy(FileTypeServiceImpl.class):fileTypeService);
		Map<String,String> mMap=new HashMap<String,String>();
		mMap.put("fileType-image:图片类型", ".jpg;.png;.bmp;.gif;.jpe;.ico");
		mMap.put("fileType-excel:excel文档", ".csv;.xls;.xlsx");
		mMap.put("fileType-word:word文档", ".doc;.dot;.docx;.dotx");
		mMap.put("fileType-ppt:演示文稿", ".ppt;.pptx");
		mMap.put("fileType-zip:压缩文档", ".zip;.rar;.gz;.7z;.war;.ear;.apk");
		mMap.put("fileType-video:流式媒体", ".flv;.wmv;.avi;.rm;.rmvb;.mpg;.mpeg;.3gp;.swf;.vod;.dat");
		mMap.put("fileType-pdf:PDF文档", ".pdf");
		mMap.put("fileType-pdf:音频文件", ".mp3;.wma");
		for(String key:mMap.keySet()){
			String icon=key.split(":")[0];
			String k=key.split(":")[1];
			String v=mMap.get(key);
			FileTypeEntity f=new FileTypeEntity();
			f.setFileTypeIcon(icon);
			f.setFileTypeName(k);
			f.setFileExts(v);
			f.setCreateTime(new Date());
			f.setUpdateTime(new Date());
			fileTypeService.saveEntity(f);
		}
	}
	
	
}
