package com.ibpd.henuocms.common.des;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class ScoreUtil {

	public static final String PROP_DIR="WEB-INF";
	public static final String CONFFILE_FILENAME="config.property";
	public static final String CONF_DB_URL="dbUrl";
	public static final String CONF_DB_USER="dbuser";
	public static final String CONF_DB_PASS="dbpass";
	public static final String CONF_DRIVER="driver";
	public static final String CONF_CARDNO_FIELDNAME="cardNoFieldName";
	public static final String CONF_SCORE_FIELDNAME="scoreFieldName";
	public static final String CONF_QUERYTABLE_TABLENAME="queryTableName";
	public static final String JSON_ISCONVERTTING_STRING="{\"cardNo\":\"0\",\"score\":\""+"Is convertting,please wait..."+"\",\"returnCode\":\"2000\"}";
	public static final String JSON_RANDERROR_STRING="{\"cardNo\":\"0\",\"score\":\""+"rand error"+"\",\"returnCode\":\"4000\"}";
	public static final String JSON_CARDNOERROR_STRING="{\"cardNo\":\"0\",\"score\":\""+"param cardNo is null"+"\",\"returnCode\":\"3000\"}";
	public static final String JSON_NORMAL_STRING="{\"cardNo\":\"$cardNo$\",\"score\":\"$score$\",\"returnCode\":\"1000\"}";
	public static final String SQL_SELECT="select * from ";
	public static final String SQL_WHERE=" where ";
	public static final String SQL_CARDNO_WHERE_SYMBO="sql_cardNo_where_symbo";
	public static final String SQL_TRANSACTION_BEGIN_NAME="sql_transaction_begin_name";
	public static final String SQL_TRANSACTION_COMMIT_NAME="sql_transaction_commit_name";
	public static final String CACHEFULLDATA="cacheFullData";
	public static final String DES_KEY="ibpd!@#$%^&*()";
	public static String getDesKey(){
		return DES_KEY;
	}
	private static Map<String,String> scoreMap=new HashMap<String,String>();
	/**
	 * 将查询到的积分信息加入到缓存里
	 * @param cardNo
	 * @param score
	 */
	public static void addScore(String cardNo,String score){
		scoreMap.put(cardNo, score);
	}
	/**
	 * 根据卡号查询缓存里是否存在积分信息，如果存在则返回，否则返回空字符串
	 * @param cardNo
	 * @return
	 */
	public static String getScore(String cardNo){
		String s=scoreMap.get(cardNo);
		s=(s==null)?"":s;
		return s.trim();
	}
//	/**
//	 * 缓存所有数据到map中
//	 */
//	public static void cacheFullData(DBHelper db1,String sql){
//        try {
//			sql = ScoreUtil.SQL_SELECT+ScoreUtil.queryTableName+ScoreUtil.SQL_WHERE+ScoreUtil.cardNoFieldName+"=-1";//SQL语句  
//	        db1 =db1==null  || db1.conn.isClosed()? new DBHelper(ScoreUtil.dbUrl,ScoreUtil.dbuser,ScoreUtil.dbpass,ScoreUtil.driver,sql):db1; 
//			ResultSet rs=db1.pst.executeQuery(sql);
//			scoreMap.clear();
//			Integer i=0;
//			while(rs.next()){
//				scoreMap.put(rs.getString(cardNoFieldName), rs.getString(scoreFieldName));
//				++i;
//			}
//			System.out.println("共将"+i+"条数据导入到内存");
//		} catch (SQLException e) {
//			System.out.println("无法获取到全部数据到缓存中");
//			e.printStackTrace();
//		}
//	}
	public static void initParam(HttpServletRequest request){
		ScoreUtil.confPath=ScoreUtil.confPath==StringUtils.EMPTY?ScoreUtil.confPath=request.getRealPath("/")+ScoreUtil.PROP_DIR+File.separator+ScoreUtil.CONFFILE_FILENAME:ScoreUtil.confPath;
		ScoreUtil.dbUrl=ScoreUtil.dbUrl==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_DB_URL):ScoreUtil.dbUrl;
		ScoreUtil.dbuser=ScoreUtil.dbuser==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_DB_USER):ScoreUtil.dbuser;
		ScoreUtil.dbpass=ScoreUtil.dbpass==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_DB_PASS):ScoreUtil.dbpass;
		ScoreUtil.driver=ScoreUtil.driver==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_DRIVER):ScoreUtil.driver;
		ScoreUtil.cardNoFieldName=ScoreUtil.cardNoFieldName==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_CARDNO_FIELDNAME):ScoreUtil.cardNoFieldName;
		ScoreUtil.scoreFieldName=ScoreUtil.scoreFieldName==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_SCORE_FIELDNAME):ScoreUtil.scoreFieldName;
		ScoreUtil.queryTableName=ScoreUtil.queryTableName==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CONF_QUERYTABLE_TABLENAME):ScoreUtil.queryTableName;
		ScoreUtil.sql_where_cardNo_symbo=ScoreUtil.sql_where_cardNo_symbo==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.SQL_CARDNO_WHERE_SYMBO):ScoreUtil.sql_where_cardNo_symbo;
		ScoreUtil.cacheFullData=ScoreUtil.cacheFullData==StringUtils.EMPTY?AcessTest.readValueByKey(ScoreUtil.confPath, ScoreUtil.CACHEFULLDATA):ScoreUtil.cacheFullData;

	}
	public static String confPath=StringUtils.EMPTY;
	public static String dbUrl=StringUtils.EMPTY;
	public static String dbuser=StringUtils.EMPTY;
	public static String dbpass=StringUtils.EMPTY;
	public static String driver=StringUtils.EMPTY;
	public static String cardNoFieldName=StringUtils.EMPTY;
	public static String scoreFieldName=StringUtils.EMPTY;
	public static String queryTableName=StringUtils.EMPTY;
	public static String sql_where_cardNo_symbo=StringUtils.EMPTY;
	public static String cacheFullData=StringUtils.EMPTY;
	public static void reset() {
		confPath=StringUtils.EMPTY;
		dbUrl=StringUtils.EMPTY;
		dbuser=StringUtils.EMPTY;
		dbpass=StringUtils.EMPTY;
		driver=StringUtils.EMPTY;
		cardNoFieldName=StringUtils.EMPTY;
		scoreFieldName=StringUtils.EMPTY;
		queryTableName=StringUtils.EMPTY;
		sql_where_cardNo_symbo=StringUtils.EMPTY;
		cacheFullData=StringUtils.EMPTY;
		scoreMap.clear();
		
	}

}
