package com.ibpd.henuocms.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.henuocms.entity.RolePermissionEntity;
import com.ibpd.shopping.assist.InterfaceUtil;

/**
 * 用户权限的扩展类
 * @author mg by qq:349070443
 *
 */
public class ExtUserPermissionEntity extends RolePermissionEntity {
	private String funName="";
	private String funCode="";
	private String modelName="";
public ExtUserPermissionEntity(RolePermissionEntity up){
	try {
		InterfaceUtil.swap(up, this);
	} catch (NoSuchMethodException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvocationTargetException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	public void setFunName(String funName) {
		this.funName = funName;
	}
	public String getFunName() {
		return funName;
	}
	public void setFunCode(String funCode) {
		this.funCode = funCode;
	}
	public String getFunCode() {
		return funCode;
	}
}
